# Package

version       = "0.1.4"
author        = "unixer"
description   = "Create telegram bots!"
license       = "MIT"
srcDir        = "src"

# Dependencies

requires "nim >= 2.0.2"

