# tbot
# Goals
This library aims to be as simple as possible.
# Examples
- `echo.nim` shows how to receive and send messages.
- `photos.nim` shows how to receive, save and send photos.
- `files.nim` shows how to receive, save and send files.

# Implemented features
- Text messages
- Photos
- Files
# Planned features
- Videos
- Voice messages

