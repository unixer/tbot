import tbot
import strformat

var bot = newBot()
while true:
  let message = bot.getMessage()
  let chat = message.chat_id
  if message.kind == photo:
    bot.sendText(chat, "Sending a photo from telegram servers'")
    bot.sendPhoto(message.chat_id, message.file_id)

    bot.sendText(chat, "Sending a photo from a remove web server")
    bot.sendPhoto(message.chat_id, "https://giphy.com/embed/IsDjNQPc4weWPEwhWm/video")

    let filename = bot.saveFile(message.file_id, "last_photo")
    echo fmt"{filename} has been saved!"
  else:
      echo "Not a photo!"
