import tbot
import strformat

var bot = newBot()
while true:
  let message = bot.getMessage()
  let chat = message.chat_id
  if message.kind == file:
    bot.sendText(chat, "Sending a file from telegram servers'")
    bot.sendFile(message.chat_id, message.file_id)

    bot.sendText(chat, "Sending a file from a remove web server")
    bot.sendFile(message.chat_id, "https://codeberg.org/unixer/tbot/archive/v0.1.0.zip")

    let filename = bot.saveFile(message.file_id, "last_file")
    echo fmt"{filename} has been saved!"
  else:
      echo "Not a file!"
