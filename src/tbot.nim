import json
import os
import httpClient
import strformat
import strutils

type TelebotException* = object of CatchableError

type Bot* = object
  client: HttpClient
  token: string
  last_update_id: int
  updates: JsonNode

type
  MessageKind* = enum
    text
    file
    photo
  Message* = object
    case kind*: MessageKind:
    of text: text*: string
    of photo, file: file_id*: string
    chat_id*: int

proc update(self: var Bot) =
  self.updates = parseJson(self.client.getContent(fmt"https://api.telegram.org/bot{self.token}/getUpdates?offset={self.last_update_id}"))

proc newBot*(token: string): Bot =
  result = Bot(client : newHttpClient(),
               token: token,
              )
  result.client.timeout = int.high
  result.update()

## Gets token from TBOT_TOKEN enviroment variable
proc newBot*(): Bot =
  let token = getEnv("TBOT_TOKEN")
  if token == "":
    raise TelebotException.newException("TBOT_TOKEN is empty. You need to specify the token.")
  result = newBot(token)

proc getMessage*(self: var Bot): Message =
  while true:
    try:
      let entries = self.updates["result"].elems
      for i in countup(0, entries.len-1):
        let update_id = entries[i]["update_id"].getInt()
        if update_id <= self.last_update_id:
          continue
        self.last_update_id = update_id

        let msg = entries[i]["message"]

        if msg.contains("document"):
          result= Message(kind: file, file_id: msg["document"]["file_id"].getStr())
        elif msg.contains("photo"):
          result= Message(kind: photo, file_id: msg["photo"][3]["file_id"].getStr())
        elif msg.contains("text"):
          result= Message(kind: text, text: msg["text"].getStr())
        else:
          echo "Message is not supported"
          continue
        result.chat_id = msg["from"]["id"].getInt()
        return
      self.update()
    except:
      raise getCurrentException()
    finally:
      self.client.close()

## File extention will be added to the filename
## Filename(with extension) is returned
proc saveFile*(self: Bot, file_id: string, filename: string): string =
  try:
    let answer = parseJson(self.client.getContent(fmt"https://api.telegram.org/bot{self.token}/getFile?file_id={file_id}"))
    if not answer["ok"].getBool():
      raise TelebotException.newException("Failed to send a message.")
    let file_path = answer["result"]["file_path"].getStr()
    result = fmt"{filename}.{file_path.rsplit('.')[^1]}"
    self.client.downloadFile(fmt"https://api.telegram.org/file/bot{self.token}/{file_path}", result)
  except:
    raise getCurrentException()
  finally:
    self.client.close()

proc sendText*(self: Bot, chat_id: int, text: string) =
  try:
    if not parseJson(self.client.getContent(fmt"https://api.telegram.org/bot{self.token}/sendMessage?chat_id={chat_id}&text={text}"))["ok"].getBool():
      raise TelebotException.newException("Failed to send a message.")
  except:
    raise getCurrentException()
  finally:
    self.client.close()


## photo - file_id or url
proc sendPhoto*(self: Bot, chat_id: int, photo: string) =
  try:
    if not parseJson(self.client.getContent(fmt"https://api.telegram.org/bot{self.token}/sendPhoto?chat_id={chat_id}&photo={photo}"))["ok"].getBool():
      raise TelebotException.newException("Failed to send a photo.")
  except:
    raise getCurrentException()
  finally:
    self.client.close()

## https://core.telegram.org/bots/api#sending-files - sending by URL will currently only work for GIF, PDF and ZIP files.
## file - file_id or url
proc sendFile*(self: Bot, chat_id: int, document: string) =
  try:
    if not parseJson(self.client.getContent(fmt"https://api.telegram.org/bot{self.token}/sendDocument?chat_id={chat_id}&document={document}"))["ok"].getBool():
      raise TelebotException.newException("Failed to send a file.")
  except:
    raise getCurrentException()
  finally:
    self.client.close()
